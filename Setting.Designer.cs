﻿namespace ScreenCapture
{
    partial class Setting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.ck_alt = new System.Windows.Forms.CheckBox();
            this.ck_ctrl = new System.Windows.Forms.CheckBox();
            this.ck_shift = new System.Windows.Forms.CheckBox();
            this.ck_autorun = new System.Windows.Forms.CheckBox();
            this.ck_capturecursor = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.ck_alt);
            this.groupBox1.Controls.Add(this.ck_ctrl);
            this.groupBox1.Controls.Add(this.ck_shift);
            this.groupBox1.Location = new System.Drawing.Point(14, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 70);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "HotKey Normal";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(175, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(46, 21);
            this.textBox1.TabIndex = 3;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // ck_alt
            // 
            this.ck_alt.AutoSize = true;
            this.ck_alt.Location = new System.Drawing.Point(127, 30);
            this.ck_alt.Name = "ck_alt";
            this.ck_alt.Size = new System.Drawing.Size(42, 16);
            this.ck_alt.TabIndex = 2;
            this.ck_alt.Text = "Alt";
            this.ck_alt.UseVisualStyleBackColor = true;
            // 
            // ck_ctrl
            // 
            this.ck_ctrl.AutoSize = true;
            this.ck_ctrl.Location = new System.Drawing.Point(73, 30);
            this.ck_ctrl.Name = "ck_ctrl";
            this.ck_ctrl.Size = new System.Drawing.Size(48, 16);
            this.ck_ctrl.TabIndex = 1;
            this.ck_ctrl.Text = "Ctrl";
            this.ck_ctrl.UseVisualStyleBackColor = true;
            // 
            // ck_shift
            // 
            this.ck_shift.AutoSize = true;
            this.ck_shift.Location = new System.Drawing.Point(13, 30);
            this.ck_shift.Name = "ck_shift";
            this.ck_shift.Size = new System.Drawing.Size(54, 16);
            this.ck_shift.TabIndex = 0;
            this.ck_shift.Text = "Shift";
            this.ck_shift.UseVisualStyleBackColor = true;
            // 
            // ck_autorun
            // 
            this.ck_autorun.AutoSize = true;
            this.ck_autorun.Location = new System.Drawing.Point(27, 105);
            this.ck_autorun.Name = "ck_autorun";
            this.ck_autorun.Size = new System.Drawing.Size(72, 16);
            this.ck_autorun.TabIndex = 1;
            this.ck_autorun.Text = "Auto Run";
            this.ck_autorun.UseVisualStyleBackColor = true;
            // 
            // ck_capturecursor
            // 
            this.ck_capturecursor.AutoSize = true;
            this.ck_capturecursor.Location = new System.Drawing.Point(141, 105);
            this.ck_capturecursor.Name = "ck_capturecursor";
            this.ck_capturecursor.Size = new System.Drawing.Size(108, 16);
            this.ck_capturecursor.TabIndex = 2;
            this.ck_capturecursor.Text = "Capture Cursor";
            this.ck_capturecursor.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(87, 134);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Setting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 169);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ck_capturecursor);
            this.Controls.Add(this.ck_autorun);
            this.Controls.Add(this.groupBox1);
            this.Name = "Setting";
            this.Text = "Setting";
            this.Load += new System.EventHandler(this.Setting_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox ck_shift;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox ck_alt;
        private System.Windows.Forms.CheckBox ck_ctrl;
        private System.Windows.Forms.CheckBox ck_autorun;
        private System.Windows.Forms.CheckBox ck_capturecursor;
        private System.Windows.Forms.Button button1;
    }
}