﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ScreenCapture.Util
{
    public class HttpClientHelper
    {
        public static HttpClient client = new HttpClient();
        /// <summary>
        /// HttPClient操作POST
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string HttpClientPost(string url, List<KeyValuePair<string, string>> data)
        {
            var content = new FormUrlEncodedContent(data);
            var response = client.PostAsync(url, content).Result;
            var responseString = response.Content.ReadAsStringAsync().Result;
            return responseString;
        }

        /// <summary>
        /// HttPClient操作POST
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string HttpClientPostImg(string url, List<KeyValuePair<string, string>> data)
        {
            string str = string.Empty;
            if (data != null)
            {
                foreach (var item in data)
                {
                    str += item.Key + "=" + item.Value;
                }
            }
            var content = new StringContent(str, Encoding.UTF8, "application/json");
            var response = client.PostAsync(url, content).Result;
            var responseString = response.Content.ReadAsStringAsync().Result;
            return responseString;
        }
    }
}
