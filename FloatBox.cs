﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace ScreenCapture
{
    public partial class FloatBox : Form
    {
        public FloatBox()
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
            this.MouseEnter += FloatBox_MouseEnter;
            this.MouseDown += FloatBox_MouseDown;
            this.MouseMove += FloatBox_MouseMove;
            this.MouseUp += FloatBox_MouseUp;
            this.MouseLeave += FloatBox_MouseLeave;
            this.MouseDoubleClick += FloatBox_MouseDoubleClick;
            m_keyHook = new KeyHook();
            screenCapture = new ScreenCapture(this);
        }
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_HOTKEY)
            {
                if (m.WParam == (IntPtr)HOTKEY_N_ID)
                    this.StartCapture(false);
            }
            base.WndProc(ref m);
        }
        //启动截图
        private void StartCapture(bool bFromClip)
        {

            //  screenCapture.IsCaptureCursor = checkBox_CaptureCursor.Checked;
            screenCapture.IsFromClipBoard = bFromClip;
            screenCapture.Show();
        }
        private void FloatBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Hide();
            ScreenCapture screenCapture = new ScreenCapture(this);
            screenCapture.Show();
        }
        private KeyHook m_keyHook;
        private bool isMouseDown = false;
        private Point mouseOffset;
        ScreenCapture screenCapture;
        private const uint WM_HOTKEY = 0x312;
        private void FloatBox_MouseLeave(object sender, EventArgs e)
        {
            //Point p = MousePosition;
            //if (p.X - 10 <= this.Left || p.X + 10 >= this.Left + miniFormWidth || p.Y - 10 <= this.Top || p.Y + 10 >= this.Bottom)
            //{
            //    isMouseEnter = false;
            //    hideDetailFormTimer.Enabled = true;
            //}
        }

        private void FloatBox_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
            this.Cursor = Cursors.Default;
        }

        private void FloatBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown == true)
            {
                Point old = this.Location;
                this.Location = getMiniBallMoveLocation();
                //if (old.X != this.Location.X || old.Y != this.Location.Y)
                //{
                //    if (bigForm != null && bigForm.Visible)
                //        hideDetailsForm();
                //}
                //else
                //{
                //    if (bigForm != null && !bigForm.Visible)
                //    {
                //        isMouseEnter = true;
                //        showDetailFormTimer.Enabled = true;
                //    }
                //}
            }
        }

        private void FloatBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = true;
                mouseOffset = new Point(MousePosition.X - this.Location.X, MousePosition.Y - this.Location.Y);
                this.Cursor = Cursors.SizeAll;
            }
        }

        private void FloatBox_MouseEnter(object sender, EventArgs e)
        {
            //  throw new NotImplementedException();
        }

        private void FloatBox_Load(object sender, EventArgs e)
        {
            this.ShowIcon = false;
            this.ControlBox = false;
            this.Top = 100;
            this.Left = Screen.PrimaryScreen.Bounds.Width - 100;
            this.Width = 80;
            this.Height = 80;
            this.FormBorderStyle = FormBorderStyle.None;
            this.TopMost = true;
            this.TransparencyKey = Color.White;
            this.BackColor = Color.White;

            m_keyHook.SetHook();
            if (!this.LoadSetting()) MessageBox.Show("load setting fail!");

            notifyIcon1.Visible = true;     //托盘来一个气泡提示
            notifyIcon1.ShowBalloonTip(30, "ScreenCapture", "ScreenCapture has started!", ToolTipIcon.Info);

        }
        private const int HOTKEY_N_ID = 1000;
        private uint m_uCtrlKey_n;
        private uint m_uAuxKey_n;
        private bool LoadSetting()
        {
            //【注意】用绝对路径 如果开机启动的话还没有完全进入系统程序就启动 使用相对路径可能无法找到文件
            if (!File.Exists(Application.StartupPath + "\\CaptureSetting.ini"))      //从文件中获取设置
                return false;
            byte[] byTemp = File.ReadAllBytes(Application.StartupPath + "\\CaptureSetting.ini");
            if (byTemp.Length != 10)
                return false;
            m_uCtrlKey_n = BitConverter.ToUInt32(byTemp, 0);
            m_uAuxKey_n = BitConverter.ToUInt32(byTemp, 4);

            Win32.UnregisterHotKey(this.Handle, HOTKEY_N_ID);  //卸载原来的热键
            if (!Win32.RegisterHotKey(this.Handle, HOTKEY_N_ID, m_uCtrlKey_n, m_uAuxKey_n))  //添加热键
                return false;

            return true;
        }
        /// <summary>
        /// 绘画按钮
        /// </summary>
        /// <param name="pe"></param>
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            Graphics g = pe.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;

            //Brush brush = new SolidBrush(Color.FromArgb(78, 78, 78));
            //Rectangle rect = new Rectangle(0, 0, 95, 38);
            //GraphicsPath path = CreateRoundedRectanglePath(rect, 19);
            //g.FillPath(brush, path);

            //brush = new SolidBrush(Color.WhiteSmoke);
            //rect = new Rectangle(0, 0, 95, 38);
            //path = CreateRoundedRectanglePath(rect, 19);
            //g.FillPath(brush, path);

            Brush brush = new SolidBrush(Color.FromArgb(51, 154, 56));
            g.FillEllipse(brush, 2, 2, 34, 34);

            brush = new SolidBrush(Color.GreenYellow);//填充的颜色         
            g.FillEllipse(brush, 3, 3, 32, 32);

            g.Dispose();
        }


        public static GraphicsPath CreateRoundedRectanglePath(Rectangle rect, int cornerRadius)
        {
            int diameter = cornerRadius * 2;
            Rectangle arcRect = new Rectangle(rect.Location, new Size(diameter, diameter));
            GraphicsPath path = new GraphicsPath();

            // 左上角
            path.AddArc(arcRect, 180, 90);

            // 右上角
            arcRect.X = rect.Right - diameter;
            path.AddArc(arcRect, 270, 90);

            // 右下角
            arcRect.Y = rect.Bottom - diameter;
            path.AddArc(arcRect, 0, 90);

            // 左下角
            arcRect.X = rect.Left;
            path.AddArc(arcRect, 90, 90);
            path.CloseFigure();//闭合曲线
            return path;
        }

        /*小球出现的位置*/
        private Point getMiniBallMoveLocation()
        {
            int x = MousePosition.X - mouseOffset.X;
            int y = MousePosition.Y - mouseOffset.Y;
            if (x < 0)
            {
                x = 0;
            }
            if (y < 0)
            {
                y = 0;
            }
            //if (Screen.PrimaryScreen.WorkingArea.Width - x < miniFormWidth)
            //{
            //    x = Screen.PrimaryScreen.WorkingArea.Width - miniFormWidth;
            //}
            //if (Screen.PrimaryScreen.WorkingArea.Height - y < miniFormHeight)
            //{
            //    y = Screen.PrimaryScreen.WorkingArea.Height - miniFormHeight;
            //}
            return new Point(x, y);
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
            // Environment.Exit(0);
        }

        private void SettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Setting setting = new Setting();
            setting.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.Hide();
            }
            else
            {
                this.Show();
            }
        }

        private void FloatBox_VisibleChanged(object sender, EventArgs e)
        {
            toolStripMenuItem1.Text = "Show Float" + (this.Visible ? "√" : "");
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }
    }
}
