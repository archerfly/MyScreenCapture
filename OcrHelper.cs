﻿using ScreenCapture.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;

namespace ScreenCapture
{
    public class OcrHelper
    {

        private static BaiduAccessToken accessToken;
        /// <summary>
        /// 普通版本
        /// </summary>
        private static readonly string GeneralUrl = "https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic";
        /// <summary>
        /// 高精版本
        /// </summary>
        private static readonly string AccurateUrl = "https://aip.baidubce.com/rest/2.0/ocr/v1/accurate_basic";

        private static string Baidu_ApiKey = "IZUIk721Gil0IKwm26rSm2Xl";
        private static string Baidu_SecretKey = "Ir26D4v5oTkAT0UOTmjH5sGlZqsg3G5K";
        /// <summary>
        ///   获取AccessToken
        /// </summary>
        /// <returns></returns>
        private static BaiduAccessToken getAccessToken()
        {
            String authHost = "https://aip.baidubce.com/oauth/2.0/token";

            List<KeyValuePair<string, string>> paraList = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials"),
                new KeyValuePair<string, string>("client_id", Baidu_ApiKey),
                new KeyValuePair<string, string>("client_secret", Baidu_SecretKey)
            };
            String result = HttpClientHelper.HttpClientPost(authHost, paraList);
            if (!string.IsNullOrEmpty(result))
            {
                BaiduAccessToken token = new BaiduAccessToken();
                token = JsonConvert.JsonToObject(result, token.GetType()) as BaiduAccessToken;
                return token;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        ///  图像识别
        /// </summary>
        /// <param name="base64str"></param>
        /// <returns></returns>
        public static OcrData GetOcrData(Bitmap bmp, bool IsGeneral)
        {
            if (accessToken == null || accessToken.ExpireTime < DateTime.Now)
            {
                accessToken = getAccessToken();
            }
            string base64str = HttpUtility.UrlEncode(ImgHelper.ImgToBase64(bmp), System.Text.Encoding.UTF8);

            string OCR_URL = (IsGeneral ? GeneralUrl : AccurateUrl) + "?access_token=" + accessToken.access_token;

            List<KeyValuePair<String, String>> paraList = new List<KeyValuePair<string, string>>()
            {
             new KeyValuePair<string, string>("image", base64str)
            };

            String result = HttpClientHelper.HttpClientPostImg(OCR_URL, paraList);
            if (!string.IsNullOrEmpty(result))
            {
                OcrData ocrResult = new OcrData();
                ocrResult = JsonConvert.JsonToObject(result, ocrResult.GetType()) as OcrData;
                return ocrResult;
            }
            else
            {
                return null;
            }
        }




    }


    public class OcrData
    {
        public string log_id { get; set; }
        public int words_result_num { get; set; }
        public List<StrWords> words_result { get; set; }
        public string error_msg { get; set; }
        public int error_code { get; set; }
    }
    public class StrWords
    {
        public string words { get; set; }
    }

    public class BaiduAccessToken
    {
        public string refresh_token { get; set; }
        public int expires_in { get; set; }
        public string scope { get; set; }
        public string session_key { get; set; }
        public string access_token { get; set; }
        public string session_secret { get; set; }
        public string error { get; set; }
        public string error_description { get; set; }
        public DateTime ExpireTime { get; set; }
    }
}
